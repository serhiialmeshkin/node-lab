import fs from 'fs'
import path from 'path';
import {fileURLToPath} from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const allowedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js']

class FileController {
    createFile(req, res, next) {
        const { filename, content } = req.body;
        const filePath = path.resolve(__dirname, `../files/${filename}`);

        if (!content) {
            res.status(400).json({ message:  `Please specify 'content' parameter`});
            return;
        }

        if (!filename) {
            res.status(400).json({ message:  `Please specify 'filename' parameter`});
            return;
        }

        const fileNameParts = filename.split('.');

        if (!allowedExtensions.includes(fileNameParts[fileNameParts.length - 1])) {
            res.status(400).json({ message: "Invalid extensions" });
            return;
        }

        if (fs.existsSync(filePath)) {
            throw new Error("File already exists")
        }

        try {
            fs.writeFileSync(filePath, content);
            res.json({
                message: "File created successfully"
            })
        } catch (e) {
            next(e)
        }
    }

    async getFiles(req, res, next) {
        const folderPath = path.resolve(__dirname, `../files/`);
        try {
            const files = await fs.promises.readdir(folderPath);
            res.status(200).json({ message: "Success", files: files });
        } catch (e) {
           return next(e);
        }
    }

    getFile(req, res, next) {
        const fileName = req.params.filename;
        const filePath = path.resolve(__dirname, `../files/` + fileName);
        if (!fs.existsSync(filePath)) {
            return res.status(400).json({message: `No file with name ${fileName} found`});
        }
        try {
            fs.readFile(filePath, (err, data) => {
                if (err) {
                    return next(err);
                }

                const fileNameParts = fileName.split('.');
                const stats = fs.statSync(filePath);
                const result = {
                    message: "Success",
                    filename: fileName,
                    content: data.toString(),
                    extension: fileNameParts[fileNameParts.length - 1],
                    uploadedDate: stats.birthtime
                }

                return res.status(200).json(result)
            });
        } catch (e) {
            return next(e)
        }
    }

    updateFile(req, res, next) {
        const fileName = req.params.filename;
        const filePath = path.resolve(__dirname, `../files/` + fileName);
        const content = req.body.content;

        if (!fs.existsSync(filePath)) {
            return res.status(400).json({message: `No file with name ${fileName} found`});
        }

        if (!content) {
            return res.status(400).json({message: `Please specify 'content' parameter` });
        }
        
        try {
            fs.writeFileSync(filePath, content);
            res.status(200).json({message: `File has been updated`})
        } catch (e) {
            return next(e)
        }
    }

    deleteFile(req, res, next) {
        const fileName = req.params.filename;
        const filePath = path.resolve(__dirname, `../files/` + fileName);

        try {
            if (!fs.existsSync(filePath)) {
                return res.status(400).json({message: `No file with name ${fileName} found`});
            }
            fs.unlinkSync(filePath);
            res.status(200).json({ message: 'File has been deleted' });
        } catch (e) {
            return next(e);
        }
    }
}

export default new FileController()