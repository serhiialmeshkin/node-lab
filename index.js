import express from 'express'
import router from './routes/index.js'
import cors from 'cors';
import logger from 'morgan';
import errorHandler from './middlewares/errorHandler.js'
import fs from 'fs'
import path from 'path';
import {fileURLToPath} from 'url';


const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const PORT = 8080;

const app = express();

app.use(express.json())
app.use(logger('dev'));
app.use(cors());

app.use('/api', router)



app.use(errorHandler);

const start = async () => {
    try {
        fs.mkdir(path.join(__dirname, 'files'),
            { recursive: true },
            (err) => {
            if (err) {
                return console.error(err);
            }
            console.log(`Directory 'files' created, if it didn't exist!`);
        });
        app.listen(PORT, () => console.log(`server started on port ${PORT}`));
    } catch (e) {
        console.log(e)
    }
}

start()
