import express from 'express'
import Router from 'express';
import FileController from "../contollers/FileController.js";

const router = new Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/files', FileController.createFile)

router.get('/files', FileController.getFiles)

router.get('/files/:filename', FileController.getFile)

router.put('/files/:filename', FileController.updateFile)

router.delete('/files/:filename', FileController.deleteFile)


export default router
